const mongoose = require("mongoose");
const env = require("dotenv").config();
const constant = require("./constant");
const connect = async () => {
  try {
    await mongoose.connect(constant.URI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    });
    console.log("mongoose connected");
  } catch (error) {
    console.log(error);

    process.exit(1);
  }
};

module.exports = connect;
