const express = require("express");
const path = require("path");
const connect = require("./config/db");
const cors = require("cors");
const env = require("dotenv").config();
const fileUpload = require("express-fileupload");
const fs = require("fs");
const app = express();
app.use(
  cors({
    origin: "*",
  })
);
app.use(fileUpload());
const p = path.dirname(process.mainModule.filename);
const dir = `${p}/client/public/uploads/`;

app.use(express.static(dir));
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}
app.use(express.static(dir));
connect();
app.use(express.json());
app.use("/api", require("./api/houssem"));
app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log("connected");
});
